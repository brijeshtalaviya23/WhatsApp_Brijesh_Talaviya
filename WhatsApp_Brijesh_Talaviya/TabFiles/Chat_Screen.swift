//
//  Chat_Screen.swift
//  WhatsApp_Brijesh_Talaviya
//
//  Created by R91 on 28/09/22.
//

import UIKit
import CoreData
import IQKeyboardManager


class Chat_Screen: UIViewController {
    
    @IBOutlet weak var Top_View: UIView!
    @IBOutlet weak var My_View: UIView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var TextFild: UITextField!
    @IBOutlet weak var Send_Button: UIButton!
    @IBOutlet weak var image_back: UIImageView!
    @IBOutlet weak var Table_View: UITableView!
    
    var chatex = ChatVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl.text = chatex.thredmodel.users[0]
        Table_View.dataSource = chatex
        Table_View.delegate = chatex
        lbl.cornerRadius = 5
        
        Table_View.cornerRadius = 15
        Table_View.layer.borderWidth = 0.5
        
        imageView.image = chatex.image
        image_back.cornerRadius = 15
        imageView.cornerRadius = 20
        
        button.cornerRadius = 10
        
        Send_Button.cornerRadius = 10
        
        TextFild.cornerRadius = 10
        Top_View.cornerRadius = 25
        My_View.cornerRadius = 25
        chatex.tab = Table_View
        chatex.getData()
        TextFild.addTarget(self, action: #selector(live), for: .editingChanged)
        
    }
   
    @objc func live() {
        TextFild.text?.trimmingCharacters(in: .whitespaces ) == "" ? (Send_Button.isSelected = false ) : (Send_Button.isSelected = true)
    }
    
    @IBAction func navigetionButton() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func onClickButton() {
        if TextFild.text != "" {
            let whiteSpace = TextFild.text!.trimmingCharacters(in: .whitespaces)
            if  whiteSpace != "" {
                chatex.insertData(tex: whiteSpace)
                TextFild.text = ""
            }
            live()

        }
    }
    
}
