//
//  Setting.swift
//  WhatsApp_Brijesh_Talaviya
//
//  Created by R93 on 24/11/22.
//

import UIKit

class Setting: UIViewController {
    
    
    
    @IBOutlet weak var My_TableView: UITableView!
    var name = ["Profile","Lock Permision"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        My_TableView.dataSource = self
        My_TableView.delegate = self
        My_TableView.layer.cornerRadius = 15
        My_TableView.layer.borderWidth = 1
        // Do any additional setup after loading the view.
        
    }
    
}
extension Setting : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        name.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = name[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let aa = storyboard!.instantiateViewController(withIdentifier: "ProfileScreen")
            self.navigationController?.pushViewController(aa, animated: true)
        } else {
            let aa = storyboard!.instantiateViewController(withIdentifier: "SwitchScreen")
            self.navigationController?.pushViewController(aa, animated: true)
        }
    }
}
