//
//  TextCell_self.swift
//  WhatsApp_Brijesh_Talaviya
//
//  Created by R91 on 28/09/22.
//

import UIKit

class TextCell_self: UITableViewCell {

    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var LblDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lbl.cornerRadius = 5
        view.cornerRadius = 8
    }
}
