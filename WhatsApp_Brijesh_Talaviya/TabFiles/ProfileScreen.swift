//
//  ProfileScreen.swift
//  WhatsApp_Brijesh_Talaviya
//
//  Created by Brijeshtalaviya on 15/12/22.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import FirebaseCore
import FirebaseStorage



class ProfileScreen: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

    @IBOutlet weak var TxtFild: UITextField!
    @IBOutlet weak var My_imageView: UIImageView!
    @IBOutlet weak var saveDataButton: UIButton!
    
    @IBOutlet weak var saveimageButton: UIButton!
    var firebase = Firestore.firestore()
//    var image =
    override func viewDidLoad() {
        super.viewDidLoad()
        My_imageView.image = UIImage(named: "photo-1615199905555-1dc4f184621a")
    }

    @IBAction func OnClickButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
   
    @IBAction func OnClickButton() {
        let storageRef = Storage.storage().reference()
        let data = Data()
        let riversRef = storageRef.child("photo-1615199905555-1dc4f184621a")
        let metadata = StorageMetadata()
        metadata.contentType = "image/png"
        riversRef.putData(data, metadata: metadata)
            
    }
    @IBAction func OnClickSave(_ sender: UIButton) {
        if TxtFild.text != "" {
            let aa = firebase.collection("Profiles").document(uidFirebase)
            aa.updateData(["name":TxtFild.text ?? "No"])
        }
       
    }
}
