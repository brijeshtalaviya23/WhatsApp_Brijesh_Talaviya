//
//  Cell.swift
//  WhatsApp_Brijesh_Talaviya
//
//  Created by R91 on 28/09/22.
//

import UIKit

class Cell: UITableViewCell {

    @IBOutlet weak var my_image: UIImageView!
    @IBOutlet weak var Lbl: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        my_image.cornerRadius = 25
       
    }

    
}
