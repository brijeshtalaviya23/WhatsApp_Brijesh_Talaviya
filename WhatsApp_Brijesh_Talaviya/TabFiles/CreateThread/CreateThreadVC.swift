//
//  CreateThreadVC.swift
//  WhatsApp_Brijesh_Talaviya
//
//  Created by Brijeshtalaviya on 28/11/22.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import FirebaseCore




class User {
    var name = ""
    var userid = ""
    init(dic:[String:Any]) {
        self.name = dic["name"] as? String ?? ""
        self.userid = dic["userId"] as? String ?? ""
    }
}

class CreateThreadVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var My_table: UITableView!
    var firebase = Firestore.firestore()
    var modelarray = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        My_table.delegate = self
        My_table.dataSource = self
        
        let fire = firebase.collection("Profiles").document(uidFirebase)
        fire.setData(["name":"User","userId":uidFirebase])
        data()
    }
    
    
    func data() {
        let doc = firebase.collection("Profiles")
        doc.whereField("userId", isNotEqualTo:uidFirebase).getDocuments { snapshot, error in
            let arr = snapshot?.documents
            for i in arr! {
                let model = User(dic: i.data())
                self.modelarray.append(model)
                self.My_table.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        modelarray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = My_table.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! My_TableCell
        cell.lbl.text = modelarray[indexPath.row].name
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let firebase = firebase.collection("Threads").document()
        firebase.setData(["docId":firebase.documentID,"titel":"--","users":[modelarray[indexPath.row].userid,uidFirebase]])
    }
}
