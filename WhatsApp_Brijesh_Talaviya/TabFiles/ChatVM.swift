//
//  chat_Extention.swift
//  WhatsApp_Brijesh_Talaviya
//
//  Created by R93 on 14/11/22.
//

import Foundation
import UIKit
import FirebaseFirestore
import FirebaseAuth
import FirebaseCore

extension UIView {
    var cornerRadius:CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
        }
    }
}

class ChatModel {
    var date = Date()
    var docId = ""
    var msg = ""
    var sender = ""
    var type = ""
    
    init(dic:[String:Any]) {
        if let time = dic["date"] as? Timestamp {
            self.date = time.dateValue()
        }
        self.docId = dic["docId"] as? String ?? ""
        self.msg = dic["msg"] as? String ?? ""
        self.sender = dic["sender"] as? String ?? ""
        self.type = dic["type"] as? String ?? ""
    }
    
}
class ChatVM:NSObject {
    
    var stringg = ""
    var myData = [ChatModel]()
    var tab:UITableView!
    var thredmodel:ThredModel!
    var timestamp = Timestamp()
    var image:UIImage?
}
extension ChatVM: UITableViewDataSource, UITableViewDelegate {
    
    
    
    func insertData(tex:String) {
        let firebase = Firestore.firestore()
        
        let fire = firebase.collection("Threads").document(thredmodel.docId).collection("Messages").document()
        let dic = [
            "date":Date(),
            "docId":fire.documentID,
            "msg":tex,
            "sender":uidFirebase,
            "type":"text",
        ] as [String : Any]
        
        fire.setData(dic)
    }
    
    func getData() {
        
        let firebase = Firestore.firestore()
        let docData = firebase.collection("Threads").document(thredmodel.docId).collection("Messages")
        
        docData.addSnapshotListener { i, error in
            self.myData = []
            let data = i?.documents
            for i in data! {
                let model = ChatModel(dic: i.data())
                self.myData.append(model)
            }
            self.myData = self.myData.sorted(by: { i, j in
                return i.date < j.date
            })
            self.tab.reloadData()
            if self.myData.isEmpty != true {
                self.tab.scrollToRow(at: IndexPath(row:self.myData.count-1, section: 0), at: .bottom, animated: true)
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        myData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let date = DateFormatter()
        date.dateFormat = "hh:mm a"
        if myData[indexPath.row].sender != uidFirebase {
            let cell = tab.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! Text_Cell
            cell.lblop.text = myData[indexPath.row].msg
            cell.lblDate.text = date.string(from: myData[indexPath.row].date)
            cell.senderLbl.text = myData[indexPath.row].docId
            if myData.count - 1 == indexPath.row || myData[indexPath.row].sender != myData[indexPath.row + 1 ].sender {
                cell.stack.isHidden = false
            } else  {
                cell.stack.isHidden = true
            }
            return cell
        } else {
            let cell = tab.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! TextCell_self
            cell.lbl.text = myData[indexPath.row].msg
            cell.LblDate.text = date.string(from: myData[indexPath.row].date)
            return cell
        }
        
    }
}
