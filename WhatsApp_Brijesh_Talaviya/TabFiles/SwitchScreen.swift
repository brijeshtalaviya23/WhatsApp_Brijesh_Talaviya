//
//  SwitchScreen.swift
//  WhatsApp_Brijesh_Talaviya
//
//  Created by R93 on 24/11/22.
//

import UIKit
import FirebaseFirestore
import FirebaseCore

class SwitchScreen: UIViewController {

    var lockscr = Lock()
    var value = 0
    var val = Lock().cellCount
    let aa = UserDefaults()


    @IBOutlet weak var onClickSwitch: UISwitch!
    @IBOutlet weak var segmentt: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        onClickSwitch.addTarget(self, action: #selector(switchButn), for: .valueChanged)
    
        segmentt.selectedSegmentIndex = aa.integer(forKey: "count")
        onClickSwitch.isOn = aa.bool(forKey: "switch")
    }
    @objc func switchButn() {
        aa.set(onClickSwitch.isOn, forKey: "switch")
        aa.synchronize()
        
    }
    @IBAction func onClickBack() {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func onClickSegment(_ sender: UISegmentedControl) {
        aa.set(sender.selectedSegmentIndex, forKey: "count")
        aa.synchronize()
        
    }
}
