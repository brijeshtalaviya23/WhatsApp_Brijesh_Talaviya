//
//  Text_Cell.swift
//  WhatsApp_Brijesh_Talaviya
//
//  Created by R91 on 28/09/22.
//

import UIKit

class Text_Cell: UITableViewCell {
    
    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var senderLbl: UILabel!
    @IBOutlet weak var im: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblop: UILabel!
    @IBOutlet weak var view: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblop.cornerRadius = 5
        view.cornerRadius = 10
        senderLbl.cornerRadius = 5
        im.cornerRadius = 15
    }
    
    
}
